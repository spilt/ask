# ask - a simple command line asker
`ask` is a simple command line tool to get user input. `ask` is less janky than
`read`, more compact than `fzf` and `fzy`, and less bloated than
readline-based tools. `ask` supports most of the typical readline-style line editing
functionality (e.g. arrow keys, backspace, Ctrl-U) and can be used to perform
fuzzy matching or basic user input all on a single line of terminal space. Like
fuzzy find tools, `ask` plays nicely with unix pipelines, but unlike the fuzzy
find tools, `ask` only uses a single line of terminal output, so it's good for
embedding. `ask`'s functionality overlaps with fuzzy finders, but if you want
to see a full list of things you're filtering through, use `fzy` or `fzf`
instead of `ask`.

## Usage
Here's a simple program to move a file from the current directory:

    #!/bin/sh
    file="`ls | ask "Pick a file: "`"
    mv "$file" "`ask "Move $file to: "`"

`ask` also supports a few other command line options:

* `ask -y` or `ask --yes` and `ask -n` or `ask --no` will append " [Y/n]" or
  " [y/N]" respectively to the prompt, and provide "Y" and "N" as the only
  options, and will exit with success or failure accordingly. (e.g. `if ask
  --yes "Continue?"; then ...`)
* `ask --quickpick` or `ask -Q` will pick an option automatically without
  pressing enter if there is only one valid option.
* `ask --password` or `ask -P` will show a typing indicator without displaying
  the typed letters on the screen. (e.g. `password="$(ask -P "Enter your
  password: ")"`)
* `ask --history=<name>` will load/save previous `ask` responses in
  `$XDG_DATA/ask/<name>.hist` (`~/.local/share/ask/<name>.hist` by default) for
  use with up/down arrow keys. Maximum of 1000 entries are stored per log file.

For the full set of command line options, run `man ./ask.1`.

## License
`ask` is released under the MIT License. See LICENSE for details.
